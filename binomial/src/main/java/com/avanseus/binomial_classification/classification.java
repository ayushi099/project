package com.avanseus.binomial_classification;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.mongodb.DBObject;
import org.bson.Document;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class classification {

    public static void main(String[] args) {
        getoutput() ;
    }

    private static void getoutput() {
        feature f = new feature();
        f.getfeatures();
        mongo_con mn= new mongo_con();
        MongoDatabase database = mn.getConnection();
        MongoCollection<Document> collection = database.getCollection("freq1");
        MongoCollection<Document> collection1 = database.getCollection("sum");


        List<Document> documents = (List<Document>) collection.find().into(new ArrayList<Document>());
        List<Document> documents1 = (List<Document>) collection1.find().into(new ArrayList<Document>());
        //String y="yes",n="no";

        double res1=proby(f.f1,f.a1,documents,documents1);
        double res2=proby(f.f2,f.a2,documents,documents1);
        double res3=proby(f.f3,f.a3,documents,documents1);
        double res4=proby(f.f4,f.a4,documents,documents1);
        double total=res1*res2*res3*res4;

        double res11=probn(f.f1,f.a1,documents,documents1);
        double res22=probn(f.f2,f.a2,documents,documents1);
        double res33=probn(f.f3,f.a3,documents,documents1);
        double res44=probn(f.f4,f.a4,documents,documents1);
        double totall=res11*res22*res33*res44;

        if(total>=totall)
            System.out.println("class is YES");
        else
            System.out.println("class is NO");

    }

    private static double proby(String f1, String a1,List<Document> documents,List<Document> documents1) {
        Object likelihood, ppp, cpp;
        double l = 0, p = 0, c = 0;
        for (int i = 0; i < documents.size(); i++) {
            Document person = documents.get(i);
            if (person.get("feature").equals(f1) && person.get("attribute").equals(a1)) {
                likelihood = person.get("yes");
                l = (Double) likelihood;
                ppp = person.get("ppp");
                p = (Double) ppp;
                System.out.println("yes:" + likelihood + " ppp:" + ppp);
            }
        }
        for (int j = 0; j < documents1.size(); j++) {
            Document person1 = documents1.get(j);
            if (person1.get("feature").equals(f1)) {
                cpp = person1.get("yes");
                c = (Double) cpp;
                System.out.println("yes:" + cpp);
            }
        }
        double result = formula(l, p, c);
        System.out.println("result is:" + result);
        return result;
    }
    private static double probn(String f1, String a1,List<Document> documents,List<Document> documents1) {
        Object likelihood, ppp, cpp;
        double l = 0, p = 0, c = 0;
        for (int i = 0; i < documents.size(); i++) {
            Document person = documents.get(i);
            if (person.get("feature").equals(f1) && person.get("attribute").equals(a1)) {
                likelihood = person.get("no");
                l = (Double) likelihood;
                ppp = person.get("ppp");
                p = (Double) ppp;
                System.out.println("no:" + likelihood + " ppp:" + ppp);
            }
        }
        for (int j = 0; j < documents1.size(); j++) {
            Document person1 = documents1.get(j);
            if (person1.get("feature").equals(f1)) {
                cpp = person1.get("no");
                c = (Double) cpp;
                System.out.println("no:" + cpp);
            }
        }
        double result = formula(l, p, c);
        System.out.println("result is:" + result);
        return result;
    }

    private static double formula(double likelihood,double ppp,double cpp) {
        double r=likelihood*ppp/cpp;
        return r;
    }
}
