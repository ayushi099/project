package com.avanseus.binomial_classification;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.mongodb.DBObject;
import org.bson.Document;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class mongo_con {

        public MongoDatabase getConnection() {
            MongoClient client = new MongoClient("localhost", 27017);
            MongoDatabase database = client.getDatabase("demodb");
            return database;
        }

    }

