package com.avanseus.textanalysis;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.mongodb.DBObject;
import org.bson.Document;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongodbFind {
   static String f1,a1,f2,a2,f3,a3,f4,a4;
    public static void main(String[] args) {

        getfeatures();
         getoutput();

    }

    private static void getoutput() {
        MongoDatabase database = getConnection();
        MongoCollection<Document> collection = database.getCollection("freq1");
        MongoCollection<Document> collection1 = database.getCollection("sum");


        List<Document> documents = (List<Document>) collection.find().into(new ArrayList<Document>());
        List<Document> documents1 = (List<Document>) collection1.find().into(new ArrayList<Document>());
        //String y="yes",n="no";
        double res1=proby(f1,a1,documents,documents1);
        double res2=proby(f2,a2,documents,documents1);
        double res3=proby(f3,a3,documents,documents1);
        double res4=proby(f4,a4,documents,documents1);
        double total=res1*res2*res3*res4;

        double res11=probn(f1,a1,documents,documents1);
        double res22=probn(f2,a2,documents,documents1);
        double res33=probn(f3,a3,documents,documents1);
        double res44=probn(f4,a4,documents,documents1);
        double totall=res11*res22*res33*res44;

        if(total>totall)
            System.out.println("class is yes");
        else
            System.out.println("class is no");

    }

    private static double proby(String f1, String a1,List<Document> documents,List<Document> documents1) {
        Object likelihood, ppp, cpp;
        double l = 0, p = 0, c = 0;
            for (int i = 0; i < documents.size(); i++) {
                Document person = documents.get(i);
                if (person.get("feature").equals(f1) && person.get("attribute").equals(a1)) {
                    likelihood = person.get("yes");
                    l = (Double) likelihood;
                    ppp = person.get("ppp");
                    p = (Double) ppp;
                    //System.out.println("yes:" + likelihood + " ppp:" + ppp);
                }
            }
            for (int j = 0; j < documents1.size(); j++) {
                Document person1 = documents1.get(j);
                if (person1.get("feature").equals(f1)) {
                    cpp = person1.get("yes");
                    c = (Double) cpp;
                    //System.out.println("yes:" + cpp);
                }
            }
            double result = formula(l, p, c);
            //System.out.println("result is:" + result);
       return result;
    }
    private static double probn(String f1, String a1,List<Document> documents,List<Document> documents1) {
        Object likelihood, ppp, cpp;
        double l = 0, p = 0, c = 0;
        for (int i = 0; i < documents.size(); i++) {
            Document person = documents.get(i);
            if (person.get("feature").equals(f1) && person.get("attribute").equals(a1)) {
                likelihood = person.get("no");
                l = (Double) likelihood;
                ppp = person.get("ppp");
                p = (Double) ppp;
                //System.out.println("no:" + likelihood + " ppp:" + ppp);
            }
        }
        for (int j = 0; j < documents1.size(); j++) {
            Document person1 = documents1.get(j);
            if (person1.get("feature").equals(f1)) {
                cpp = person1.get("no");
                c = (Double) cpp;
                //System.out.println("no:" + cpp);
            }
        }
        double result = formula(l, p, c);
        //System.out.println("result is:" + result);
        return result;
    }
    private static void getfeatures() {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter features and attribute:");
         f1=sc.next();
         a1=sc.next();
         f2=sc.next();
         a2=sc.next();
         f3=sc.next();
         a3=sc.next();
         f4=sc.next();
         a4=sc.next();

    }

    private static MongoDatabase getConnection() {
        MongoClient client = new MongoClient("localhost", 27017);
        MongoDatabase database = client.getDatabase("demodb");
        return database;
    }
    private static double formula(double likelihood,double ppp,double cpp) {
       double r=likelihood*ppp/cpp;
        return r;
    }
}